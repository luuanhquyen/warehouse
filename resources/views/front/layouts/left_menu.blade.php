<h5 class="sidebar-title">Menu</h5>
<ul class="nav nav-pills nav-stacked nav-quirk">
    @foreach ($categories as $category) 
        <li class="{{ Request::is('/product') || Request::is('/') ? "" : "" }}"><a href="/product_category/{{ $category['id'] }}"><i class="fa fa-home"></i> <span>{{$category['name']}}</span></a></li>
    @endforeach
    <li ><a href="/admin"><span class="badge pull-right"></span><i class="fa fa-cube"></i> <span>Administrator Site</span></a></li>
    
</ul>

