<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="">
  <!--<link rel="shortcut icon" href="/images/favicon.png" type="image/png">-->

  <title>Warehouse management</title>

  <link rel="stylesheet" href="/lib/Hover/hover.css">
  <link rel="stylesheet" href="/lib/fontawesome/css/font-awesome.css">
  <link rel="stylesheet" href="/lib/weather-icons/css/weather-icons.css">
  <link rel="stylesheet" href="/lib/ionicons/css/ionicons.css">
  <link rel="stylesheet" href="/lib/jquery-toggles/toggles-full.css">
  <link rel="stylesheet" href="/lib/morrisjs/morris.css">

  <link rel="stylesheet" href="/css/quirk.css">
  <link rel="stylesheet" href="/lib/jquery.steps/jquery.steps.css">
  <script src="/lib/modernizr/modernizr.js"></script>

  <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!--[if lt IE 9]>
  <script src="/lib/html5shiv/html5shiv.js"></script>
  <script src="/lib/respond/respond.src.js"></script>
  <![endif]-->
</head>

<body>

<header>
  <div class="headerpanel">

    <div class="logopanel">
      <h2><a href="index.html">HGD-demo</a></h2>
    </div><!-- logopanel -->

    <div class="headerbar">

      <a id="menuToggle" class="menutoggle"><i class="fa fa-bars"></i></a>

      <div class="searchpanel">
        <div class="input-group">
            <!--
          <input type="text" class="form-control" placeholder="Search for...">
          <span class="input-group-btn">
            <button class="btn btn-default" type="button"><i class="fa fa-search"></i></button>
          </span>
            -->
        </div><!-- input-group -->
      </div>

      <div class="header-right">
        <ul class="headermenu">
          <li>
            <div id="noticePanel" class="btn-group">
              <button class="btn btn-notice alert-notice" data-toggle="dropdown">
                <i class="fa fa-globe"></i>
              </button>
             
            </div>
          </li>
          <li>
            <div class="btn-group">
              <button type="button" class="btn btn-logged" data-toggle="dropdown">
                <img src="/images/photos/loggeduser.png" alt="" />
               Admin
                <span class="caret"></span>
              </button>
             
            </div>
          </li>
          <li>
            <button id="chatview" class="btn btn-chat alert-notice">
              <span class="badge-alert"></span>
              <i class="fa fa-comments-o"></i>
            </button>
          </li>
        </ul>
      </div><!-- header-right -->
    </div><!-- headerbar -->
  </div><!-- header-->
</header>

<section>

  <div class="leftpanel">
    <div class="leftpanelinner">

      <!-- ################## LEFT PANEL PROFILE ################## -->

      <div class="media leftpanel-profile">
        <div class="media-left">
          <a href="#">
            <img src="/images/photos/loggeduser.png" alt="" class="media-object img-circle">
          </a>
        </div>
        <div class="media-body">
          <h4 class="media-heading">Admin <a data-toggle="collapse" data-target="#loguserinfo" class="pull-right"><i class="fa fa-angle-down"></i></a></h4>
          <span>Administrator</span>
        </div>
      </div><!-- leftpanel-profile -->

      

     
      <div class="tab-content">

        <!-- ################# MAIN MENU ################### -->
        <div class="tab-pane active" id="mainmenu">
            @include('front.layouts.left_menu')
        </div><!-- tab-pane -->
      </div><!-- tab-content -->

    </div><!-- leftpanelinner -->
  </div><!-- leftpanel -->

  <div class="mainpanel">

    <!--<div class="pageheader">
      <h2><i class="fa fa-home"></i> Dashboard</h2>
    </div>-->

    <div class="contentpanel">


        @yield('content')
 

    </div><!-- contentpanel -->

  </div><!-- mainpanel -->

</section>

<script src="/lib/jquery/jquery.js"></script>
<script src="/lib/jquery-ui/jquery-ui.js"></script>
<script src="/lib/bootstrap/js/bootstrap.js"></script>
<script src="/lib/jquery-toggles/toggles.js"></script>

<script src="/lib/morrisjs/morris.js"></script>
<script src="/lib/raphael/raphael.js"></script>

<script src="/lib/flot/jquery.flot.js"></script>
<script src="/lib/flot/jquery.flot.resize.js"></script>
<script src="/lib/flot-spline/jquery.flot.spline.js"></script>

<script src="/lib/jquery-knob/jquery.knob.js"></script>

<script src="/js/quirk.js"></script>
<script src="/lib/datatables/jquery.dataTables.js"></script>
<script src="/lib/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.js"></script> 
<script src="/lib/jquery-toggles/toggles.js"></script>
<script src="/lib/jquery.steps/jquery.steps.js"></script>
<script src="/lib/jquery-validate/jquery.validate.js"></script>
@yield('script')
</body>
</html>
