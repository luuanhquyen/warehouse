
@extends('front.layouts.layout')

@section('content')





<div class="row">
    <div class="col-sm-8 col-md-9 col-lg-10 people-list">
        <div class="row">
            @foreach ($products as $product) 

            <div class="col-md-6 col-lg-3">
                <div class="panel panel-profile grid-view">
                    <div class="panel-heading">
                        <div class="text-center">
                            <img src="{{$product['photo']}}" style="height: 125px">
                            <h4 class="panel-profile-name">{{$product['title']}}</h4>
                            <p class="media-usermeta">{{$product['detail']}}</p>
                        </div>
                        
                    </div><!-- panel-heading -->
                    <div class="panel-body people-info">
                        @foreach ($product['products_categories_properties'] as $pcp) 
                        <div class="info-group">
                            <label>{{$pcp['categories_properties']['property']['name']}}</label>
                            {{$pcp['value']}} {{$pcp['categories_properties']['property']['unit']}}
                        </div>
                        @endforeach
                    </div><!-- panel-body -->
                </div><!-- panel -->
            </div><!-- col-md-6 -->
            @endforeach


        </div><!-- row -->

    </div><!-- col-sm-8 -->

    <div class="col-sm-4 col-md-3 col-lg-2">
        <form action="" method="POST">
        <div class="panel">
            <div class="panel-heading">
                <h4 class="panel-title">Filter Products</h4>
            </div>
    
            <div class="panel-body">              
                <div class="form-group">
                    @foreach ($category['categories_properties'] as $cp) 
                    @if($cp['property']['data_type']=='enum')
                    <label class="control-label center-block">{{$cp['property']['name']}}:</label>
                    @foreach ($cp['options'] as $o) 
                    <label class="ckbox ckbox-inline mr20">
                        <input @if ($o['checked']==1) checked='checked' @endif  type="checkbox" value="{{$o['value']}}" name="categories_properties[{{$cp['id']}}][]" ><span>{{$o['value']}} {{$cp['property']['unit']}}</span>
                    </label>
                    @endforeach
                    <hr>
                    @endif
                    
                    @endforeach
                </div>
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <button type="submit" class="btn btn-success btn-block">Filter List</button>
            </div>
        </div><!-- panel -->
        </form>
        

        
    </div>
</div><!-- row -->


@endsection
@section('script')



@endsection