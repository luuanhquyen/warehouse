<h5 class="sidebar-title">Menu</h5>
          <ul class="nav nav-pills nav-stacked nav-quirk">
            <li class="{{ Request::is('admin/product*') || Request::is('/') ? "active" : "" }}"><a href="/admin/product"><i class="fa fa-home"></i> <span>Products</span></a></li>
            <li class="{{ Request::is('admin/category*') || Request::is('/') ? "active" : "" }}"><a href="/admin/category"><span class="badge pull-right"></span><i class="fa fa-cube"></i> <span>Categories</span></a></li>
            <li class="{{ Request::is('admin/property*') || Request::is('/') ? "active" : "" }}"><a href="/admin/property"><i class="fa fa-map-marker"></i> <span>Properties</span></a></li>
            <li ><a href="/"><i class="fa fa-map-marker"></i> <span>Front site</span></a></li>
          </ul>

         