
@extends('admin.layouts.layout')
           
@section('content')
    


<div class="panel">
        <div class="panel-heading">
          <h4 class="panel-title">Manage properties
          </h4>
            <p>
                Manage properties for categories
            </p>
            <a href="/admin/property/add" class="btn btn-primary btn-sm">Add new property</a>
        </div>
        <div class="panel-body">
          <div class="table-responsive">
            
            <table id="exRowTable" class="table table-bordered table-striped-col">
              <thead>
                <tr>
                  <th></th>
                  <th>Name</th>
                  <th>Date Type</th>
                  <th>Low limit</th>
                  <th>High limit</th>
                  <th>Action</th>
                </tr>
              </thead>
            </table>
          </div>
        </div>
      </div><!-- panel -->
      
      

@endsection
@section('script')

     
      <script>
$(document).ready(function() {
  'use strict';
var i=0;
 var exRowTable = $('#exRowTable').DataTable({
    responsive: true,
    
    'fnDrawCallback': function(oSettings) {
      $('#exRowTable_paginate ul').addClass('pagination-active-success');
    },
    'ajax': '/api/property',
    'columns': [{
      'class': 'details-control',
      'orderable': false,
      'data': null,
      'defaultContent': ''
    },
    { 'data': 'name' },
    { 'data': 'data_type' },
    { 'data': 'low_limit' },
    { 'data': 'high_limit'},
    {
                mRender: function (data, type, row) {
            return "<a href='/admin/property/edit/"+row.id+"'>Edit</a> | <a href='/admin/property/delete/"+row.id+"'>Delete</a>";
        }
            }
    ],

    'order': [[1, 'asc']],
    initComplete: function () {
            this.api().columns().every( function () {
                i=i+1;
                if(i==3)
                {
                var column = this;
                var select = $('<select style="margin-left:10px" class="form-control"><option value="">All data type</option></select>')
                    .appendTo( $(".dataTables_length") )
                    .on( 'change', function () {
                        var val = $.fn.dataTable.util.escapeRegex(
                            $(this).val()
                        );
 
                        column
                            .search( val ? '^'+val+'$' : '', true, false )
                            .draw();
                    } );
 
                column.data().unique().sort().each( function ( d, j ) {
                    select.append( '<option value="'+d+'">'+d+'</option>' )
                } );
            }
                
                
                
            } );
        }
  });

  // Add event listener for opening and closing details
  $('#exRowTable tbody').on('click', 'td.details-control', function () {
    var tr = $(this).closest('tr');
    var row = exRowTable.row( tr );

    if ( row.child.isShown() ) {
      // This row is already open - close it
      row.child.hide();
      tr.removeClass('shown');
    } else {
      // Open this row
      row.child( format(row.data()) ).show();
      tr.addClass('shown');
    }
  });

  function format (d) {

    return '<h4>'+d.name+'<small></small></h4>'+
    '<p class="nomargin"> Description:'+d.note+'</p>'+
    '<p class="nomargin"> Min:'+d.low_limit+'</p>'+
    '<p class="nomargin"> Max:'+d.high_limit+'</p>'+
    '<p class="nomargin"> Value: '+d.value+'</p>'; 
  }


});
</script>

@endsection