@extends('admin.layouts.layout')
@section('content')

<div class="panel">
    <div class="panel">
        <div class="panel-heading">
            <h4 class="panel-title">Add new property </h4>
            <p></p>
        </div>
        <div class="panel-body">

            <!-- Nav tabs -->
            <ul class="nav nav-tabs nav-line">
                <li class="active"><a href="#popular11" data-toggle="tab"><strong>Property</strong></a></li>

            </ul>

            <!-- Tab panes -->
            <div class="tab-content">
                <div class="tab-pane active" id="popular11">

                    <form id="basicForm" method="post" action="" class="form-horizontal">
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Name:<span class="text-danger">*</span></label>
                            <div class="col-sm-8">
                                <input type="text" value='' name="name" class="form-control" placeholder="Name of property..." required />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Description:<span class="text-danger">*</span></label>
                            <div class="col-sm-8">
                                <input type="text" value='' name="note" class="form-control" placeholder="Description of property..." required />
                            </div>
                        </div>


                        <div class="form-group">
                            <label class="col-sm-3 control-label">Data Type <span class="text-danger">*</span></label>
                            <div class="col-sm-8">
                                <select id="data_type" name='data_type' class='data_type form-control'>
                                    <option value="integer">Interger Number</option>
                                    <option value="float">Float Number</option>
                                    <option value="string">String</option>
                                    <option value="textarea">Textarea</option>
                                    <option value="richtext">Rich text</option>
                                    <option value="enum">Enums</option>
                                </select>
                            </div>
                        </div>
                        <hr>
                        <p>Additional options</p>

 
                        <div id='additional_enum' class="form-group additional">
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Options for enum<span class="text-danger">*</span></label>
                                <div class="col-sm-8">
                                    <input name='value' class="form-control" type='text' value='' placeholder="Enter options seperate by ; character example: yellow, blue, white, red">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Unit<span class="text-danger">*</span></label>
                                <div class="col-sm-8">
                                    <input name='unit' class="form-control" type='text' value='' placeholder="example: mm">
                                </div>
                            </div>
                        </div>


                        <div class="form-group additional" id='additional_integer'>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">min value<span class="text-danger">*</span></label>
                                <div class="col-sm-8">
                                    <input name='low_limit' class="form-control" type='text' value='' placeholder="Min value ">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">max value<span class="text-danger">*</span></label>
                                <div class="col-sm-8">
                                    <input name='high_limit' class="form-control" type='text' value='' placeholder="Max value ">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Unit<span class="text-danger">*</span></label>
                                <div class="col-sm-8">
                                    <input name='unit' class="form-control" type='text' value='' placeholder="example: mm">
                                </div>
                            </div>
                        </div>
                        <div class="form-group additional" id='additional_float'>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">min value<span class="text-danger">*</span></label>
                                <div class="col-sm-8">
                                    <input name='low_limit' class="form-control" type='text' value='' placeholder="Min value ">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">max value<span class="text-danger">*</span></label>
                                <div class="col-sm-8">
                                    <input name='high_limit' class="form-control" type='text' value='' placeholder="Max value ">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Unit<span class="text-danger">*</span></label>
                                <div class="col-sm-8">
                                    <input name='unit' class="form-control" type='text' value='' placeholder="example: mm">
                                </div>
                            </div>
                        </div>
                        <div class="form-group additional" id='additional_string'>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">min characters<span class="text-danger">*</span></label>
                                <div class="col-sm-8">
                                    <input name='low_limit' class="form-control" type='text' value='' placeholder="Min value ">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">max characters<span class="text-danger">*</span></label>
                                <div class="col-sm-8">
                                    <input name='high_limit' class="form-control" type='text' value='' placeholder="Max value ">
                                </div>
                            </div>
                        </div>
                        <div class="form-group additional" id='additional_textarea'>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">min characters<span class="text-danger">*</span></label>
                                <div class="col-sm-8">
                                    <input name='low_limit' class="form-control" type='text' value='' placeholder="Min value ">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">max characters<span class="text-danger">*</span></label>
                                <div class="col-sm-8">
                                    <input name='high_limit' class="form-control" type='text' value='' placeholder="Max value ">
                                </div>
                            </div>
                        </div>
                        <div class="form-group additional" id='additional_richtext'>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">min words<span class="text-danger">*</span></label>
                                <div class="col-sm-8">
                                    <input name='low_limit' class="form-control" type='text' value='' placeholder="Min value ">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">max words<span class="text-danger">*</span></label>
                                <div class="col-sm-8">
                                    <input name='high_limit' class="form-control" type='text' value='' placeholder="Max value ">
                                </div>
                            </div>
                        </div>

                        <hr>

                        <div class="row">
                            <div class="col-sm-9 col-sm-offset-3">
                                <input type="hidden" name="type" value="product">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <button type='submit' class="btn btn-success btn-quirk btn-wide mr5">Add</button>
                                <button type="reset" class="btn btn-quirk btn-wide btn-default">Reset</button>
                            </div>
                        </div>

                    </form>


                </div>


            </div>

        </div>
    </div><!-- panel -->

</div><!-- panel -->



@endsection
@section('script')

<script type="text/javascript">
    $(".additional :input").attr("disabled", true);
    $(".additional").hide();
    $("#additional_" + $("#data_type").val()).show(500);
    $("#additional_" + $("#data_type").val() + " :input").attr("disabled", false);
    $(".data_type").change(function () {

        $(".additional :input").attr("disabled", true);
        $(".additional").hide();
        $("#additional_" + this.value).show(500);
        $("#additional_" + this.value + " :input").attr("disabled", false);

    }) 
</script>

@endsection