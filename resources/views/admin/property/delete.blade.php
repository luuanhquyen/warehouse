@extends('admin.layouts.layout')
@section('content')

<div class="panel">
    <div class="panel">
        <div class="panel-heading">
            <h4 class="panel-title">{{$property['name']}} </h4>
            <p>Delete {{$property['name']}} will delete this property to all categories and all products.</p>
        </div>
        <div class="panel-body">

            <!-- Nav tabs -->
            <ul class="nav nav-tabs nav-line">
                <li class="active"><a href="#popular11" data-toggle="tab"><strong>Property</strong></a></li>
            </ul>

            <!-- Tab panes -->
            <div class="tab-content">
                <div class="tab-pane active" id="popular11">

                    <form id="basicForm" method="post" action="" class="form-horizontal">
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Name:<span class="text-danger">*</span></label>
                            <div class="col-sm-8">
                                <input disabled="" type="text" value='{{$property['name']}}' name="name" class="form-control" placeholder="Property name..." required />
                            </div>
                        </div>

                        <hr>

                        <div class="row">
                            <div class="col-sm-9 col-sm-offset-3">
                                <input type="hidden" name="delete" value="1">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <button type='submit' class="btn btn-success btn-quirk btn-wide mr5">Delete this?</button>
                            </div>
                        </div>

                    </form>


                </div>
                
            </div>

        </div>
    </div><!-- panel -->

</div><!-- panel -->



@endsection
@section('script')



@endsection