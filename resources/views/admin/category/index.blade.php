
@extends('admin.layouts.layout')
           
@section('content')
    


<div class="panel">
        <div class="panel-heading">
          <h4 class="panel-title">Manage Categories
          </h4>
          <p>Add, edit, delete categories</p>
        <a href="/admin/category/add" class="btn btn-primary btn-sm">Add new category</a>
        </div>
        <div class="panel-body">
          <div class="table-responsive">
            <table id="exRowTable" class="table table-bordered table-striped-col">
              <thead>
                <tr>
                  <th></th>
                  <th>Name</th>

                  <th>Action</th>
                </tr>
              </thead>
            </table>
          </div>
        </div>
      </div><!-- panel -->
      
      

@endsection
@section('script')

     
      <script>
$(document).ready(function() {
 
  'use strict';

 var exRowTable = $('#exRowTable').DataTable({
    responsive: true,
    'fnDrawCallback': function(oSettings) {
      $('#exRowTable_paginate ul').addClass('pagination-active-success');
    },
    'ajax': '/api/category',
    'columns': [{
      'class': 'details-control',
      'orderable': false,
      'data': null,
      'defaultContent': ''
    },
    { 'data': 'name' },

    {
                mRender: function (data, type, row) {
            return "<a href='/admin/category/edit/"+row.id+"'>Edit</a> | <a href='/admin/category/delete/"+row.id+"'>Delete</a>";
        }
            }
    ],

    'order': [[1, 'asc']]
  });

  // Add event listener for opening and closing details
  $('#exRowTable tbody').on('click', 'td.details-control', function () {
    var tr = $(this).closest('tr');
    var row = exRowTable.row( tr );

    if ( row.child.isShown() ) {
      // This row is already open - close it
      row.child.hide();
      tr.removeClass('shown');
    } else {
      // Open this row
      row.child( format(row.data()) ).show();
      tr.addClass('shown');
    }
  });

  function format (d) {
    // `d` is the original data object for the row
    var properties="";
    $.each( d.categories_properties, function( key, cp ) {
    properties+=cp.property.name+"<br />";
    });
    return '<h4>'+d.name+'<small></small></h4>'+
    '<p class="nomargin">Properties belong to this category:<br />'+properties+'</p>';
  }


});
</script>

@endsection