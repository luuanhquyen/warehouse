@extends('admin.layouts.layout')
@section('content')

<div class="panel">
    <div class="panel">
        <div class="panel-heading">
            <h4 class="panel-title">{{$category['name']}} </h4>
            <p>Edit category, add or remove properties of category</p>
        </div>
        <div class="panel-body">

            <!-- Nav tabs -->
            <ul class="nav nav-tabs nav-line">
                <li class="active"><a href="#popular11" data-toggle="tab"><strong>Product</strong></a></li>
                <li><a href="#recent11" data-toggle="tab"><strong>Properties</strong></a></li>
            </ul>

            <!-- Tab panes -->
            <div class="tab-content">
                <div class="tab-pane active" id="popular11">

                    <form id="basicForm" method="post" action="" class="form-horizontal">
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Name:<span class="text-danger">*</span></label>
                            <div class="col-sm-8">
                                <input type="text" value='{{$category['name']}}' name="name" class="form-control" placeholder="Type your name..." required />
                            </div>
                        </div>

                  

                        <hr>

                        <div class="row">
                            <div class="col-sm-9 col-sm-offset-3">
                                <input type="hidden" name="type" value="category">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <button type='submit' class="btn btn-success btn-quirk btn-wide mr5">Edit</button>
                                <button type="reset" class="btn btn-quirk btn-wide btn-default">Reset</button>
                            </div>
                        </div>

                    </form>


                </div>
                <div class="tab-pane" id="recent11">
                    <form id="basicForm" method="post" action="" class="form-horizontal">
                        
                        @foreach ($properties as $p)
                        @php ($exist = 0)
                            @foreach ($category['categories_properties'] as $cp)
                                @if ($cp['property_id'] == $p['id'])
                                    @php ($exist = 1)
                                    <input style='height:20px' checked='checked' name='properties[{{$p['id']}}]' type='checkbox' class='form-control'>{{$p['name']}}</input>
                                @endif
                            @endforeach
                            @if ($exist == 0)
                                    <input style='height:20px'  name='properties[{{$p['id']}}]' type='checkbox' class='form-control'>{{$p['name']}}</input>
                            @endif
                        @endforeach
                      

                        <hr>

                        <div class="row">
                            <div class="col-sm-9 col-sm-offset-3">
                                <input type="hidden" name="type" value="properties">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <button type='submit' class="btn btn-success btn-quirk btn-wide mr5">Edit</button>
                                <button type="reset" class="btn btn-quirk btn-wide btn-default">Reset</button> 
                            </div>
                        </div>

                    </form>
                </div>

            </div>

        </div>
    </div><!-- panel -->

</div><!-- panel -->



@endsection
@section('script')



@endsection