
@extends('admin.layouts.layout')
           
@section('content')
    


<div class="panel">
        <div class="panel-heading">
          <h4 class="panel-title">Products
          </h4>
          <p>Add, edit, delete products</p>
        <a href="/admin/product/add" class="btn btn-primary btn-sm">Add new product</a>
        </div>
        <div class="panel-body">
          <div class="table-responsive">
            <table id="exRowTable" class="table table-bordered table-striped-col">
              <thead>
                <tr>
                  <th></th>
                  <th>Name</th>
                  <th>Category</th>
                  <th>Quantity</th>
                  <th>Price</th>
                  <th>Action</th>
                </tr>
              </thead>
            </table>
          </div>
        </div>
      </div><!-- panel -->
      
      

@endsection
@section('script')

     
      <script>
$(document).ready(function() {
 
  'use strict';
var i=0;
 var exRowTable = $('#exRowTable').DataTable({
     
    responsive: true,
    'fnDrawCallback': function(oSettings) {
      $('#exRowTable_paginate ul').addClass('pagination-active-success');
    },
    'ajax': '/api/product',
    'columns': [{
      'class': 'details-control',
      'orderable': false,
      'data': null,
      'defaultContent': ''
    },
    { 'data': 'title' },
    { 'data': 'category.name' },
    { 'data': 'quantity' },
    { 'data': 'price'},
    {
                mRender: function (data, type, row) {
            return "<a href='/admin/product/edit/"+row.id+"'>Edit</a> | <a href='/admin/product/delete/"+row.id+"'>Delete</a>";
        }
            }
    ],

    'order': [[1, 'asc']],
        initComplete: function () {
            this.api().columns().every( function () {
                i=i+1;
                if(i==3)
                {
                var column = this;
                var select = $('<select style="margin-left:10px" class="form-control"><option value="">All Category</option></select>')
                    .appendTo( $(".dataTables_length") )
                    .on( 'change', function () {
                        var val = $.fn.dataTable.util.escapeRegex(
                            $(this).val()
                        );
 
                        column
                            .search( val ? '^'+val+'$' : '', true, false )
                            .draw();
                    } );
 
                column.data().unique().sort().each( function ( d, j ) {
                    select.append( '<option value="'+d+'">'+d+'</option>' )
                } );
            }
                
                
                
            } );
        }
  });

  // Add event listener for opening and closing details
  $('#exRowTable tbody').on('click', 'td.details-control', function () {
    var tr = $(this).closest('tr');
    var row = exRowTable.row( tr );

    if ( row.child.isShown() ) {
      // This row is already open - close it
      row.child.hide();
      tr.removeClass('shown');
    } else {
      // Open this row
      row.child( format(row.data()) ).show();
      tr.addClass('shown');
    }
  });

  function format (d) {
    // `d` is the original data object for the row
    var properties="";
    properties+="Price:"+d.price+"<br />";
    properties+="Quantity:"+d.quantity+"<br />";
    $.each( d.products_categories_properties, function( key, pcp ) {
    properties+=pcp.categories_properties.property.name+":"+pcp.value+" "+pcp.categories_properties.property.unit+"<br />";
    });
    return '<h4>'+d.title+'<small>'+d.detail+'</small></h4>'+
    '<p class="nomargin">Detail attributes on this product:<br />'+properties+'</p>';
  }

    
  
  
  
  
});

</script>

@endsection