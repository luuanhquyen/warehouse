@extends('admin.layouts.layout')
@section('content')

<div class="panel">
    <div class="panel">
        <div class="panel-heading">
            <h4 class="panel-title">{{$product['title']}} </h4>
            <p>Delete {{$product['category']['name']}}?</p>
        </div>
        <div class="panel-body">

            <!-- Nav tabs -->
            <ul class="nav nav-tabs nav-line">
                <li class="active"><a href="#popular11" data-toggle="tab"><strong>Product</strong></a></li>
                <li><a href="#recent11" data-toggle="tab"><strong>Properties</strong></a></li>
            </ul>

            <!-- Tab panes -->
            <div class="tab-content">
                <div class="tab-pane active" id="popular11">

                    <form id="basicForm" method="post" action="" class="form-horizontal">
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Name:<span class="text-danger">*</span></label>
                            <div class="col-sm-8">
                                <input disabled="" type="text" value='{{$product['title']}}' name="title" class="form-control" placeholder="Type your name..." required />
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label">Detail <span class="text-danger">*</span></label>
                            <div class="col-sm-8">
                                <textarea  disabled="" name='detail' rows="5" class="form-control" placeholder="Type your comment..." required>{{$product['detail']}}</textarea>
                            </div>
                        </div>

                        


                        <hr>

                        <div class="row">
                            <div class="col-sm-9 col-sm-offset-3">
                                <p>Do You really want to delete this item?</p>
                                <input type="hidden" name="delete" value="1">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <button type='submit' class="btn btn-success btn-quirk btn-wide mr5">Delete?</button>

                            </div>
                        </div>

                    </form>


                </div>
                <div class="tab-pane" id="recent11">
                    <form id="basicForm" method="post" action="" class="form-horizontal">
                        @foreach ($categoryProperties as $cp)
                        @php ($exist = 0)
                        <div class="form-group">
                            <label class="col-sm-3 control-label">{{$cp['property']['name']}}<span class="text-danger">*</span></label>
                            <div class="col-sm-8">
                                @foreach ($product['products_categories_properties'] as $pcp)
                                    @if ($pcp['category_property_id'] == $cp['id'])
                                    @php ($exist = 1)
                                    <input  disabled="" type="text" value='{{$pcp['value']}}' name="properties[{{$cp['id']}}]" class="form-control" placeholder="Enter value..." required />
                                    @endif
                                @endforeach
                                @if ($exist == 0)
                                    <input  disabled="" type="text" value='' name="properties[{{$cp['id']}}]" class="form-control" placeholder="Enter value..." required />
                                @endif
                            </div>
                        </div>
                        @endforeach
                      

                        <hr>

                        <div class="row">
                            <div class="col-sm-9 col-sm-offset-3">
                                
                            </div>
                        </div>

                    </form>
                </div>

            </div>

        </div>
    </div><!-- panel -->

</div><!-- panel -->



@endsection
@section('script')



@endsection