@extends('admin.layouts.layout')
@section('content')

<div class="panel">
    <div class="panel">
        <div class="panel-heading">
            <h4 class="panel-title">Product</h4>
            <p>Add new product</p>
        </div>
        <div class="panel-body">

            <!-- Nav tabs -->
            <ul class="nav nav-tabs nav-line">
                <li class="active"><a href="#popular11" data-toggle="tab"><strong>Product</strong></a></li>
                <li><a href="#recent11" data-toggle="tab"><strong>Properties</strong></a></li>
            </ul>

            <!-- Tab panes -->
            <div class="tab-content">
                <div class="tab-pane active" id="popular11">

                    <form id="basicForm" method="post" action="" class="form-horizontal">
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Photo:<span class="text-danger">*</span></label>
                            <div class="col-sm-8">
                                <img onclick="$('#fileInput').click();" id="preview" src="/images/camera-icon.png" style="max-height: 125px; cursor: pointer"> <br />
                                <input type="text" value='/images/camera-icon.png' id="photo" name="photo" class="form-control" placeholder="Type your name..." required />
                                <p id="state"></p>
                                <input style="display: none" type="file" id="fileInput">
                                
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Name:<span class="text-danger">*</span></label>
                            <div class="col-sm-8">
                                <input type="text" value='' name="title" class="form-control" placeholder="Type your name..." required />
                            </div>
                        </div>


                        <div class="form-group">
                            <label class="col-sm-3 control-label">Detail <span class="text-danger">*</span></label>
                            <div class="col-sm-8">
                                <textarea name='detail' rows="5" class="form-control" placeholder="Type your comment..." required></textarea>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label">Category <span class="text-danger">*</span></label>
                            <div class="col-sm-8">
                                <select name='category' class='form-control'>
                                    @foreach ($categories as $category)
                                    <option value='{{$category['id']}}'>{{$category['name']}}</option>
                                    @endforeach

                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Quantity <span class="text-danger">*</span></label>
                            <div class="col-sm-8">
                                <input type="text" value='' name="quantity" class="form-control" placeholder="Quantity..." required />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Price:<span class="text-danger">*</span></label>
                            <div class="col-sm-8">
                                <input type="text" value='' name="price" class="form-control" placeholder="Price..." required />
                            </div>
                        </div>

                        <hr>

                        <div class="row">
                            <div class="col-sm-9 col-sm-offset-3">
                                <input type="hidden" name="type" value="product">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <button id="submit" type='submit' class="btn btn-success btn-quirk btn-wide mr5">Add</button>
                                <button type="reset" class="btn btn-quirk btn-wide btn-default">Reset</button>
                            </div>
                        </div>

                    </form>


                </div>
                <div class="tab-pane" id="recent11">
                    <p>Properties only available after new producted added</p>
                </div>

            </div>

        </div>
    </div><!-- panel -->

</div><!-- panel -->



@endsection
@section('script')

<script type="text/javascript">
    $("#fileInput").change(function () {
        
        var fileInput = document.getElementById('fileInput');
        var file = fileInput.files[0];
        var imageType = /image.*/;
        if (file.type.match(imageType)) {
            var reader = new FileReader();
            $("#state").html("uploading...");
            reader.onload = function (e) {
                $("#preview").attr("src", reader.result)
                $.post("/upload", {data: reader.result.match(/,(.*)$/)[1],"fileName":file.name })
                        .done(function (data) {
                            $("#photo").val(data.path);
                            if(data.path)
                            {
                            $("#state").html("uploaded");
                            }
                        });
            }

            reader.readAsDataURL(file);
        } else {
            alert("File not supported!");
        }
    })
</script>

@endsection