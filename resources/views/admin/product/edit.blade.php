@extends('admin.layouts.layout')
@section('content')

<div class="panel">
    <div class="panel">
        <div class="panel-heading">
            <h4 class="panel-title">{{$product['title']}} </h4>
            <p>{{$product['category']['name']}}</p>
        </div>
        <div class="panel-body">

            <!-- Nav tabs -->
            <ul class="nav nav-tabs nav-line">
                <li class="active"><a href="#popular11" data-toggle="tab"><strong>Product</strong></a></li>
                <li><a href="#recent11" data-toggle="tab"><strong>Properties</strong></a></li>
            </ul>

            <!-- Tab panes -->
            <div class="tab-content">
                <div class="tab-pane active" id="popular11">

                    <form id="basicForm" method="post" action="" class="form-horizontal">
                        
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Photo:<span class="text-danger">*</span></label>
                            <div class="col-sm-8">
                                @if($product['photo'])
                            
                                <img onclick="$('#fileInput').click();" id="preview" src="{{$product['photo']}}" style="max-height: 125px; cursor: pointer"> 
                                <input type="hidden" value='{{$product['photo']}}' id="photo" name="photo" class="form-control" placeholder="Type your name..." required />
                                @else
                                <img onclick="$('#fileInput').click();" id="preview" src="/images/camera-icon.png" style="max-height: 125px; cursor: pointer"> 
                                <input type="hidden" value='/images/camera-icon.png' id="photo" name="photo" class="form-control" placeholder="Type your name..." required />
                                @endif
                                <br />
                                <p id="state"></p>
                                
                                <input style="display: none" type="file" id="fileInput">
                                
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Name:<span class="text-danger">*</span></label>
                            <div class="col-sm-8">
                                <input type="text" value='{{$product['title']}}' name="title" class="form-control" placeholder="Type your name..." required />
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label">Detail <span class="text-danger">*</span></label>
                            <div class="col-sm-8">
                                <textarea name='detail' rows="5" class="form-control" placeholder="Type your comment..." required>{{$product['detail']}}</textarea>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label">Category <span class="text-danger">*</span></label>
                            <div class="col-sm-8">
                                <select name='category' class='form-control'>
                                    @foreach ($categories as $category)
                                    <option @if ($product['category_id'] == $category['id']) selected='selected' @endif  value='{{$category['id']}}'>{{$category['name']}}</option>
                                    @endforeach

                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label">Quantity <span class="text-danger">*</span></label>
                            <div class="col-sm-8">
                                <input type="text" value='{{$product['quantity']}}' name="quantity" class="form-control" placeholder="Quantity..." required />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Price <span class="text-danger">*</span></label>
                            <div class="col-sm-8">
                                <input type="text" value='{{$product['price']}}' name="price" class="form-control" placeholder="Price..." required />
                            </div>
                        </div>
                        <hr>

                        <div class="row">
                            <div class="col-sm-9 col-sm-offset-3">
                                <input type="hidden" name="type" value="product">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <button type='submit' class="btn btn-success btn-quirk btn-wide mr5">Edit</button>
                                <button type="reset" class="btn btn-quirk btn-wide btn-default">Reset</button>
                            </div>
                        </div>

                    </form>


                </div>
                <div class="tab-pane" id="recent11">
                    <form id="basicForm" method="post" action="" class="form-horizontal">
                        @foreach ($categoryProperties as $cp)
                        @php ($exist = 0)
                        <div class="form-group">
                            <label class="col-sm-3 control-label">{{$cp['property']['name']}}<span class="text-danger">*</span></label>
                            <div class="col-sm-8">
                                @foreach ($product['products_categories_properties'] as $pcp)
                                    @if ($pcp['category_property_id'] == $cp['id'])
                                    @php ($exist = 1)
                                    @if ($cp['property']['data_type']=='string')
                                    <input type="text" value='{{$pcp['value']}}' name="properties[{{$cp['id']}}]" class="form-control" placeholder="Enter value..." required />
                                    @endif
                                    @if ($cp['property']['data_type']=='float')
                                    <input type="text" value='{{$pcp['value']}}' name="properties[{{$cp['id']}}]" class="form-control" placeholder="Enter value..." required />
                                    @endif
                                    @if ($cp['property']['data_type']=='integer')
                                    <input type="text" value='{{$pcp['value']}}' name="properties[{{$cp['id']}}]" class="form-control" placeholder="Enter value..." required />
                                    @endif
                                    @if ($cp['property']['data_type']=='textarea')
                                    <textarea  name="properties[{{$cp['id']}}]" class="form-control" placeholder="Enter value..." required />
                                    {{$pcp['value']}}
                                    </textarea>
                                    @endif
                                    @if ($cp['property']['data_type']=='richtext')
                                    <textarea   name="properties[{{$cp['id']}}]" class="form-control" placeholder="Enter value..." required />
                                    {{$pcp['value']}}
                                    </textarea>
                                    @endif
                                    
                                    @if ($cp['property']['data_type']=='enum')
                                    @php
                                    $options = explode(';',$cp['property']['value']);
                                    @endphp
                                    <select class="form-control" name="properties[{{$cp['id']}}]">
                                        @foreach($options as $option)
                                        <option @if ($pcp['value'] == $option) selected='selected' @endif value='{{$option}}'>{{$option}} {{$cp['property']['unit']}}</option>
                                        @endforeach
                                    </select>
                                    @endif
                                    
                                    
                                    @endif
                                @endforeach  
                                @if ($exist == 0)
                                    @if ($cp['property']['data_type']=='string')
                                    <input type="text" value='' name="properties[{{$cp['id']}}]" class="form-control" placeholder="Enter value..." required />
                                    @endif
                                    @if ($cp['property']['data_type']=='float')
                                    <input type="text" value='' name="properties[{{$cp['id']}}]" class="form-control" placeholder="Enter value..." required />
                                    @endif
                                    @if ($cp['property']['data_type']=='integer')
                                    <input type="text" value='' name="properties[{{$cp['id']}}]" class="form-control" placeholder="Enter value..." required />
                                    @endif
                                    @if ($cp['property']['data_type']=='textarea')
                                    <textarea name="properties[{{$cp['id']}}]" class="form-control" placeholder="Enter value..." required />
                                    
                                    </textarea>
                                    @endif
                                    @if ($cp['property']['data_type']=='richtext')
                                    <textarea name="properties[{{$cp['id']}}]" class="form-control" placeholder="Enter value..." required />
                                    
                                    </textarea>
                                    @endif
                                    
                                    @if ($cp['property']['data_type']=='enum')
                                    @php
                                    $options = explode(';',$cp['property']['value']);
                                    @endphp
                                    <select class="form-control" name="properties[{{$cp['id']}}]">
                                        @foreach($options as $option)
                                        <option  value='{{$option}}'>{{$option}} {{$cp['property']['unit']}}</option>
                                        @endforeach
                                    </select>
                                    @endif
                                
                                    
                                @endif
                                
                            </div>
                        </div>
                        @endforeach
                      

                        <hr>

                        <div class="row">
                            <div class="col-sm-9 col-sm-offset-3">
                                <input type="hidden" name="type" value="properties">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <button type='submit' class="btn btn-success btn-quirk btn-wide mr5">Edit</button>
                                <button type="reset" class="btn btn-quirk btn-wide btn-default">Reset</button> 
                            </div>
                        </div>

                    </form>
                </div>

            </div>

        </div>
    </div><!-- panel -->

</div><!-- panel -->



@endsection
@section('script')
<script type="text/javascript">
    $("#fileInput").change(function () {
        
        var fileInput = document.getElementById('fileInput');
        var file = fileInput.files[0];
        var imageType = /image.*/;
        if (file.type.match(imageType)) {
            var reader = new FileReader();
            $("#state").html("uploading...");
            reader.onload = function (e) {
                $("#preview").attr("src", reader.result)
                $.post("/upload", {data: reader.result.match(/,(.*)$/)[1],"fileName":file.name })
                        .done(function (data) {
                            $("#photo").val(data.path);
                            if(data.path)
                            {
                            $("#state").html("uploaded");
                            }
                        });
            }

            reader.readAsDataURL(file);
        } else {
            alert("File not supported!");
        }
    })
</script>

@endsection