<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
/*
Route::get('/', function () {
    return view('welcome');
});
 */
Route::get('/', 'front\ProductController@index');
Route::any('/product_category/{id}', 'front\ProductController@category');
Route::get('admin', 'admin\ProductController@admin');
Route::get('admin/product', 'admin\ProductController@index');
Route::any('admin/product/edit/{id}', 'admin\ProductController@edit');
Route::any('admin/product/add', 'admin\ProductController@create');
Route::any('admin/product/delete/{id}', 'admin\ProductController@destroy');


Route::get('admin/category', 'admin\CategoryController@index');
Route::any('admin/category/edit/{id}', 'admin\CategoryController@edit');
Route::any('admin/category/add', 'admin\CategoryController@create');
Route::any('admin/category/delete/{id}', 'admin\CategoryController@destroy');

Route::get('admin/property', 'admin\PropertyController@index');
Route::any('admin/property/edit/{id}', 'admin\PropertyController@edit');
Route::any('admin/property/add', 'admin\PropertyController@create');
Route::any('admin/property/delete/{id}', 'admin\PropertyController@destroy');

Route::any('/upload', 'CommonController@upload');