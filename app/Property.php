<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Property extends Model
{
    protected $fillable = ['name','data_type','low_limit','high_limit','note','value','unit']; 
    /**
     * Get the categories for the property.
     */
    public function categories_properties()
    {
        return $this->hasMany('App\CategoryProperty');
    }
}
