<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CategoryProperty extends Model
{
    protected $fillable = ['is_require','property_id','category_id'];
    public function category() {
        return $this->belongsTo(Category::class,'category_id');
    }
    public function property() {
        return $this->belongsTo(Property::class,'property_id');
    }
}
