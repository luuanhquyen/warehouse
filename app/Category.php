<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    /**
     * Get the products for the product.
     */
    public function products()
    {
        return $this->hasMany('App\Product');
    }
    /**
     * Get the properties for the product.
     */
    public function categories_properties()
    {
        return $this->hasMany('App\CategoryProperty');
    }
}
