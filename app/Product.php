<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = ['title','detail','category_id','photo'];
    /**
     * Get the products for the blog post.
     */
    public function products_categories_properties()
    {
        return $this->hasMany('App\ProductCategoryProperty');
    }
    public function category() {
        return $this->belongsTo(Category::class,'category_id');
    }
}
