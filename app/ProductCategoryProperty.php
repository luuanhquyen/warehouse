<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductCategoryProperty extends Model
{
    protected $fillable = ['value','product_id','category_property_id'];
    public function products() {
        return $this->belongsTo(Product::class,'product_id');
    }
    public function categories_properties() {
        return $this->belongsTo(CategoryProperty::class,'category_property_id');
    }
}
