<?php

namespace App\Http\Controllers\front;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Product;
use App\Category;
use App\CategoryProperty;
use App\ProductCategoryProperty;
use Symfony\Component\Console\Input\Input;
class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Category::get();
        $products=Product::with("products_categories_properties.categories_properties.property","category")->get();
        return view('front/product/index', ['products'=>$products,'categories'=>$categories]);
    }
    
    public function category(Request $request,$id)
    {
        $categories = Category::get();
        $category = Category::where("id",$id)->with("categories_properties")->first();
        foreach ($category->categories_properties as $index=>$cp)
        {
            $options = ProductCategoryProperty::select('value')->where("category_property_id",$cp['id'])->groupBy('value')->get();
            $category->categories_properties[$index]->options=$options;
        }
        $filters = $request->all();
        
        $products=Product::where("category_id",$id)->with("products_categories_properties.categories_properties.property","category")->get();
        $filteredProducts=array();
        if(isset($filters['categories_properties']))
        {
        if(count($filters['categories_properties'])>0)
        {
            foreach ($products as $index=>$product)
            {
                $valid=array();
                foreach ($filters['categories_properties'] as $index=>$cp)
                {
                    $valid[$index]=0;
                    foreach ($product->products_categories_properties as $pcp)
                    {
                        
                        if ($pcp['category_property_id']==$index)
                        {
                            if (in_array($pcp['value'], $cp))
                            {
                                $valid[$index]=1;
                                
                            }
                        }
                    }
                }
                $is_valid=1;
                 
                foreach ($valid as $v)
                {
                    if ($v==0)
                    {
                        $is_valid=0;
                    }
                }
                if($is_valid){$filteredProducts[] =$product;}
            }
        }
        else {
            $filteredProducts=$products;
        }
        }
        else
        {
            $filteredProducts=$products;
        }
        
        foreach ($category['categories_properties'] as $index=>$cp)
        {
        
            if (isset($filters['categories_properties']))
            {
                if (isset($filters['categories_properties'][$cp['id']]))
                {
                
                    foreach ($cp->options as $index2=>$option)
                    {
                        if (in_array($option->value, $filters['categories_properties'][$cp['id']]))
                        {
                            $category['categories_properties'][$index]->options[$index2]['checked']="1";
                        }
                    }
                    
                }
            }
        }

        return view('front/product/category', ['products'=>$filteredProducts,'categories'=>$categories,'category'=>$category,'filters'=>$filters]);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $categories=Category::get();
        if ($request->isMethod('post')) {
            $newData = $request->all();
            $id = Product::insertGetId(
                ['title' => $newData['title'],'detail'=>$newData['detail'],'category_id'=>$newData['category'],'photo'=>$newData['photo'],'quantity'=>$newData['quantity'],'price'=>$newData['price']]
            );
            return redirect('admin/product/edit/'.$id); 
        } 
        return view('admin/product/create',['categories'=>$categories]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id, Request $request)
    {
        $product=Product::where("id",$id)->with("products_categories_properties.categories_properties.property","category")->first();
        $categories=Category::get();
        if ($request->isMethod('post')) {
            $newData = $request->all();die("X");
            if ($newData['type']=='product')
            {
            $product->title=$newData['title'];
            $product->detail=$newData['detail'];
            $product->category_id=$newData['category'];
            $product->photo=$newData['photo'];
            $product->quantity=$newData['quantity'];
            $product->price=$newData['price'];
            }
            if ($newData['type']=='properties')
            {
                foreach ($newData['properties'] as $index=>$property)
                {
                    ProductCategoryProperty::updateOrCreate(
                        ['category_property_id' => $index, 'product_id' => $id],  
                        ['value' => $property]
                    );
                }
            }
            $product->save(); 
            
        } 
        $categoryProperties = CategoryProperty::Where("category_id",$product->category_id)->with("category","property")->get(); 
        $product=Product::where("id",$id)->with("products_categories_properties.categories_properties.property","category")->first();
        return view('admin/product/edit', ['product'=>$product,'categories'=>$categories,'categoryProperties'=>$categoryProperties]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id)
    {
        $product=Product::where("id",$id)->with("products_categories_properties.categories_properties.property","category")->first();
        $categories=Category::get();
        if ($request->isMethod('post')) {
            $newData = $request->all();
            if ($newData['delete']==1)
            {
            Product::where("id",$id)->delete();
            ProductCategoryProperty::where("product_id",$id)->delete();
            return redirect('admin/product');
            
            }   
        } 
        $categoryProperties = CategoryProperty::Where("category_id",$product->category_id)->with("category","property")->get(); 
        return view('admin/product/delete', ['product'=>$product,'categories'=>$categories,'categoryProperties'=>$categoryProperties]);
    }
    public function admin()
    {
        return redirect('admin/product');
    }
}
