<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
class CommonController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function upload(Request $request)
    {
        $newData = $request->all();
        $data=$newData['data'];
        $file = base64_decode($data);
        $fileName=$newData['fileName'];
        $fp = fopen('/www/hgd.vn/public/photos/'.$fileName, 'w');
        fwrite($fp, $file); 
        fclose($fp);
        return response()->json(array("path"=>"/photos/".$fileName)); 
    }

    
}
