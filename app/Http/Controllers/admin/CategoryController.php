<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Product;
use App\Category;
use App\Property;
use App\CategoryProperty;
use App\ProductCategoryProperty;
use Symfony\Component\Console\Input\Input;
class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products=Product::with("products_categories_properties.categories_properties.property","category")->get();
        return view('admin/category/index', $products);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        if ($request->isMethod('post')) {
            $newData = $request->all();
            $id = Category::insertGetId(
                ['name' => $newData['name']]
            );
            return redirect('admin/category/edit/'.$id);
        } 
        return view('admin/category/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id, Request $request)
    {
        $category=Category::where("id",$id)->with("categories_properties")->first();
        $properties=Property::get();
        if ($request->isMethod('post')) {
            $newData = $request->all();
            if ($newData['type']=='category')
            {
            $category->name=$newData['name'];
            $category->save(); 
            }
            if ($newData['type']=='properties')
            {
                $addedProperty = array();
                foreach ($newData['properties'] as $index=>$property)
                { 
                    $addedProperty[]=$index;
                    CategoryProperty::updateOrCreate(
                        ['category_id' => $id, 'property_id' => $index],
                        ['category_id' => $id, 'property_id' => $index,'is_require'=>1]
                    );
                }
                foreach ($category->categories_properties as $cp)
                {
                    if (!in_array($cp->property_id, $addedProperty))
                    {
                        CategoryProperty::where("category_id",$id)->where("property_id",$cp->property_id)->delete();
                        //echo $cp->property_id;
                    }
                }
            
            }
            return redirect('admin/category');
            
        } 
        $category=Category::where("id",$id)->with("categories_properties")->first();
        return view('admin/category/edit', ['category'=>$category,'properties'=>$properties]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id)
    {
        $category=Category::where("id",$id)->with("categories_properties")->first();
        $properties=Property::get();
        if ($request->isMethod('post')) {
            $newData = $request->all();
            if ($newData['delete']=='1')
            {
            Category::where("id",$id)->delete();
            Product::where("category_id",$id)->delete();
            CategoryProperty::where("category_id",$id)->delete();
            foreach ($category->categories_properties as $cp)
            {
              ProductCategoryProperty::where("category_property_id",$cp->id)->delete();
            }
            return redirect('admin/category'); 
            }
            
        } 
        $category=Category::where("id",$id)->with("categories_properties")->first();
        return view('admin/category/delete', ['category'=>$category,'properties'=>$properties]);
    }
}
