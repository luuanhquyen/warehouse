<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Product;
use App\Category;
use App\Property;
use App\CategoryProperty;
use App\ProductCategoryProperty;
use Symfony\Component\Console\Input\Input;
class PropertyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products=Product::with("products_categories_properties.categories_properties.property","category")->get();
        return view('admin/property/index', $products);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
         if ($request->isMethod('post')) {
            $newData = $request->all();
            if (!isset($newData['unit'])) {$newData['unit']="";}
            if (!isset($newData['value'])) {$newData['value']="";}
            Property::updateOrCreate(
                        ['name' => $newData['name']],
                        ['name' => $newData['name'],'note'=>$newData['note'],'data_type'=>$newData['data_type'],'low_limit'=>"0",'high_limit'=>"100",'value' => $newData['value'],'unit' => $newData['unit']]
                    );
   
            return redirect('admin/property');
        } 
        return view('admin/property/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id, Request $request)
    {
        $property=Property::where("id",$id)->first();
        if ($request->isMethod('post')) {
            $newData = $request->all();
            $property->name=$newData['name'];
            $property->data_type=$newData['data_type'];
            $property->note=$newData['note'];

            if(isset($newData['low_limit']))
            {
                $property->low_limit=$newData['low_limit'];
            }
            if(isset($newData['high_limit']))
            {
                $property->high_limit=$newData['high_limit'];
            }
        
            if(isset($newData['unit']))
            {
                $property->unit=$newData['unit'];
            }
            if(isset($newData['value']))
            {
                $property->value=$newData['value'];
            }
            $property->save();
            return redirect('admin/property');
        } 
        return view('admin/property/edit', ['property'=>$property]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id)
    {
        $property=Property::where("id",$id)->first();
        if ($request->isMethod('post')) {
            $newData = $request->all();
            if ($newData['delete']==1)
            {
            foreach ($property->categories_properties as $cp)
            {
                ProductCategoryProperty::where("category_property_id",$cp->id)->delete();
            }
            Property::where("id",$id)->delete();
            CategoryProperty::where("property_id",$id)->delete();
            return redirect('admin/property');
            
            }  
     
        } 
        return view('admin/property/delete', ['property'=>$property]);
    }
}
