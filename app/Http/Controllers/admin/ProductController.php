<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Product;
use App\Category;
use App\CategoryProperty;
use App\ProductCategoryProperty;
use Symfony\Component\Console\Input\Input;
class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products=Product::with("products_categories_properties.categories_properties.property","category")->get();
        return view('admin/product/index', $products);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $categories=Category::get();
        if ($request->isMethod('post')) {
            $newData = $request->all();
            $id = Product::insertGetId(
                ['title' => $newData['title'],'detail'=>$newData['detail'],'category_id'=>$newData['category'],'quantity'=>$newData['quantity'],'price'=>$newData['price'],'photo'=>$newData['photo']]
            );
            return redirect('admin/product/edit/'.$id);
        } 
        return view('admin/product/create',['categories'=>$categories]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id, Request $request)
    {
        $product=Product::where("id",$id)->with("products_categories_properties.categories_properties.property","category")->first();
        $categories=Category::get();
        if ($request->isMethod('post')) {
            $newData = $request->all();
            if ($newData['type']=='product')
            {
            $product->title=$newData['title'];
            $product->detail=$newData['detail'];
            $product->category_id=$newData['category'];
            $product->quantity=$newData['quantity'];
            $product->price=$newData['price'];
            $product->photo=$newData['photo']; 
            }
            if ($newData['type']=='properties')
            {
                foreach ($newData['properties'] as $index=>$property)
                { 
                    if ($property==NULL) {$property="";}
                    ProductCategoryProperty::updateOrCreate(
                        ['category_property_id' => $index, 'product_id' => $id],  
                        ['value' => $property]
                    );
                }
            }
            $product->save(); 
            return redirect('admin/product');
        } 
        $categoryProperties = CategoryProperty::Where("category_id",$product->category_id)->with("category","property")->get(); 
        $product=Product::where("id",$id)->with("products_categories_properties.categories_properties.property","category")->first();
        return view('admin/product/edit', ['product'=>$product,'categories'=>$categories,'categoryProperties'=>$categoryProperties]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id)
    {
        $product=Product::where("id",$id)->with("products_categories_properties.categories_properties.property","category")->first();
        $categories=Category::get();
        if ($request->isMethod('post')) {
            $newData = $request->all();
            if ($newData['delete']==1)
            {
            Product::where("id",$id)->delete();
            ProductCategoryProperty::where("product_id",$id)->delete();
            return redirect('admin/product');
            
            }   
        } 
        $categoryProperties = CategoryProperty::Where("category_id",$product->category_id)->with("category","property")->get(); 
        return view('admin/product/delete', ['product'=>$product,'categories'=>$categories,'categoryProperties'=>$categoryProperties]);
    }
    public function admin()
    {
        return redirect('admin/product');
    }
}
