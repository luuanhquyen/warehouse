-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Máy chủ: localhost
-- Thời gian đã tạo: Th8 23, 2017 lúc 06:12 AM
-- Phiên bản máy phục vụ: 5.7.19-0ubuntu0.16.04.1-log
-- Phiên bản PHP: 7.0.22-0ubuntu0.16.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `hgd.vn`
--

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `categories`
--

CREATE TABLE `categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `categories`
--

INSERT INTO `categories` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Laptop PC', NULL, NULL),
(2, 'Desktop PC', NULL, NULL),
(3, 'Máy tính xách tay', NULL, NULL);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `category_properties`
--

CREATE TABLE `category_properties` (
  `id` int(10) UNSIGNED NOT NULL,
  `category_id` int(11) NOT NULL,
  `property_id` int(11) NOT NULL,
  `is_require` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `category_properties`
--

INSERT INTO `category_properties` (`id`, `category_id`, `property_id`, `is_require`, `created_at`, `updated_at`) VALUES
(1, 2, 1, 1, '2017-08-11 01:26:06', '2017-08-11 01:26:06'),
(2, 2, 2, 1, '2017-08-11 01:26:06', '2017-08-11 01:26:06'),
(3, 2, 3, 1, '2017-08-11 01:26:06', '2017-08-11 01:26:06'),
(4, 1, 1, 1, '2017-08-11 01:26:13', '2017-08-11 01:26:13'),
(5, 1, 2, 1, '2017-08-11 01:26:13', '2017-08-11 01:26:13'),
(6, 1, 3, 1, '2017-08-11 01:26:13', '2017-08-11 01:26:13'),
(7, 1, 4, 1, '2017-08-11 01:26:13', '2017-08-11 01:26:13'),
(8, 2, 5, 1, '2017-08-11 01:35:41', '2017-08-11 01:35:41'),
(9, 1, 5, 1, '2017-08-11 01:35:47', '2017-08-11 01:35:47'),
(10, 3, 1, 1, '2017-08-11 13:54:41', '2017-08-11 13:54:41');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `photos`
--

CREATE TABLE `photos` (
  `id` int(10) UNSIGNED NOT NULL,
  `path` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `products_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `products`
--

CREATE TABLE `products` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `detail` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `photo` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `quantity` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `price` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `products`
--

INSERT INTO `products` (`id`, `title`, `detail`, `photo`, `quantity`, `category_id`, `created_at`, `updated_at`, `price`) VALUES
(1, 'Asus Zenbook UX410UA-GV063', 'Asus Zenbook UX410UA-GV063', '/photos/1.jpg', 5, 1, NULL, NULL, 599),
(2, 'Dell Inspiron 5548 i7-5500U', 'Dell Inspiron 5548 i7-5500U', '/photos/2.jpg', 5, 1, NULL, NULL, 799),
(3, 'Dell Inspiron 15-5559', 'Dell Inspiron 15-5559', '/photos/3.jpg', 5, 1, NULL, NULL, 799),
(4, 'HP Pavilion 15-AU123 Core™ i5', 'HP Pavilion 15-AU123 Core™ i5', '/photos/4.jpg', 4, 1, NULL, NULL, 699),
(5, 'Asus Vivobook S15', 'Asus Vivobook S15', '/photos/5.jpg', 3, 1, NULL, NULL, 688),
(6, 'Dell XPS 13 2016 i5', 'Dell XPS 13 2016 i5', '/photos/6.jpg', 8, 1, NULL, NULL, 789),
(7, 'Omega E8400', 'Omega E8400', '/photos/7.jpg', 4, 2, NULL, NULL, 688),
(8, 'Dell Optiplex 3046 Micro', 'Dell Optiplex 3046 Micro', '/photos/8.jpg', 5, 2, NULL, NULL, 599),
(9, 'Intel NUC Kit NUC5PGYH', 'Intel NUC Kit NUC5PGYH', '/photos/9.jpg', 6, 2, NULL, NULL, 345),
(10, 'Dell Inspiron 3650MT', 'Dell Inspiron 3650MT', '/photos/10.jpg', 4, 2, NULL, NULL, 488),
(11, 'HP Pavilion 570-p013l', 'HP Pavilion 570-p013l', '/photos/11.jpg', 4, 2, NULL, NULL, 499),
(12, '34234', 'ềwge', '/photos/background.png', 3, 1, NULL, NULL, 4340000),
(13, 'bon', 'abc', '/photos/IMG_0664.jpg', 5, 1, NULL, '2017-08-21 15:26:28', 5);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `product_category_properties`
--

CREATE TABLE `product_category_properties` (
  `id` int(10) UNSIGNED NOT NULL,
  `product_id` int(11) NOT NULL,
  `category_property_id` int(11) NOT NULL,
  `value` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `product_category_properties`
--

INSERT INTO `product_category_properties` (`id`, `product_id`, `category_property_id`, `value`, `created_at`, `updated_at`) VALUES
(1, 1, 4, '32', '2017-08-11 01:32:24', '2017-08-11 01:32:24'),
(2, 1, 5, 'Intel', '2017-08-11 01:32:24', '2017-08-11 01:32:24'),
(3, 1, 6, 'Intel i3', '2017-08-11 01:32:24', '2017-08-11 01:32:24'),
(4, 1, 7, '14', '2017-08-11 01:32:24', '2017-08-11 01:32:24'),
(5, 2, 4, '512', '2017-08-11 01:33:41', '2017-08-11 01:33:41'),
(6, 2, 5, 'Intel', '2017-08-11 01:33:41', '2017-08-11 01:36:07'),
(7, 2, 6, 'Intel i3', '2017-08-11 01:33:41', '2017-08-11 01:36:07'),
(8, 2, 7, '15.6', '2017-08-11 01:33:41', '2017-08-11 01:33:41'),
(9, 3, 4, '256', '2017-08-11 01:34:43', '2017-08-11 01:34:43'),
(10, 3, 5, 'AMD', '2017-08-11 01:34:43', '2017-08-11 01:37:40'),
(11, 3, 6, 'opteron 6700', '2017-08-11 01:34:43', '2017-08-11 01:34:43'),
(12, 3, 7, '14', '2017-08-11 01:34:43', '2017-08-11 01:34:43'),
(13, 1, 9, '6', '2017-08-11 01:35:53', '2017-08-11 01:35:53'),
(14, 3, 9, '4', '2017-08-11 01:35:59', '2017-08-11 01:35:59'),
(15, 2, 9, '8', '2017-08-11 01:36:07', '2017-08-11 01:36:07'),
(16, 4, 4, '64', '2017-08-11 01:37:17', '2017-08-11 01:37:17'),
(17, 4, 5, 'Intel', '2017-08-11 01:37:17', '2017-08-11 01:37:17'),
(18, 4, 6, 'Intel i7', '2017-08-11 01:37:17', '2017-08-11 01:37:17'),
(19, 4, 7, '15.6', '2017-08-11 01:37:17', '2017-08-11 01:37:17'),
(20, 4, 9, '12', '2017-08-11 01:37:17', '2017-08-11 01:37:17'),
(21, 5, 4, '128', '2017-08-11 01:39:32', '2017-08-11 01:39:32'),
(22, 5, 5, 'AMD', '2017-08-11 01:39:32', '2017-08-11 01:39:32'),
(23, 5, 6, 'opteron 6300', '2017-08-11 01:39:32', '2017-08-11 01:39:32'),
(24, 5, 7, '14', '2017-08-11 01:39:32', '2017-08-11 01:39:32'),
(25, 5, 9, '4', '2017-08-11 01:39:32', '2017-08-11 01:39:32'),
(26, 6, 4, '128', '2017-08-11 01:41:33', '2017-08-11 01:41:33'),
(27, 6, 5, 'AMD', '2017-08-11 01:41:33', '2017-08-11 01:41:33'),
(28, 6, 6, 'opteron 6700', '2017-08-11 01:41:33', '2017-08-11 01:41:33'),
(29, 6, 7, '13', '2017-08-11 01:41:33', '2017-08-11 01:41:33'),
(30, 6, 9, '4', '2017-08-11 01:41:33', '2017-08-11 01:41:33'),
(31, 7, 1, '512', '2017-08-11 01:42:54', '2017-08-11 01:42:54'),
(32, 7, 2, 'AMD', '2017-08-11 01:42:54', '2017-08-11 01:46:08'),
(33, 7, 3, 'opteron 6300', '2017-08-11 01:42:54', '2017-08-11 01:46:08'),
(34, 7, 8, '8', '2017-08-11 01:42:54', '2017-08-11 01:42:54'),
(35, 8, 1, '2048', '2017-08-11 01:43:55', '2017-08-11 01:43:55'),
(36, 8, 2, 'Intel', '2017-08-11 01:43:55', '2017-08-11 01:43:55'),
(37, 8, 3, 'Inter i5', '2017-08-11 01:43:55', '2017-08-11 01:43:55'),
(38, 8, 8, '4', '2017-08-11 01:43:55', '2017-08-11 01:43:55'),
(39, 9, 1, '64', '2017-08-11 01:44:43', '2017-08-11 01:44:43'),
(40, 9, 2, 'Intel', '2017-08-11 01:44:43', '2017-08-11 01:44:43'),
(41, 9, 3, 'Intel i7', '2017-08-11 01:44:43', '2017-08-11 01:44:43'),
(42, 9, 8, '2', '2017-08-11 01:44:43', '2017-08-11 01:44:43'),
(43, 10, 1, '128', '2017-08-11 01:45:43', '2017-08-11 01:45:43'),
(44, 10, 2, 'Intel', '2017-08-11 01:45:43', '2017-08-11 01:45:43'),
(45, 10, 3, 'Intel i7', '2017-08-11 01:45:43', '2017-08-11 01:45:43'),
(46, 10, 8, '6', '2017-08-11 01:45:43', '2017-08-11 01:45:43'),
(47, 11, 1, '256', '2017-08-11 01:47:18', '2017-08-11 01:47:18'),
(48, 11, 2, 'Intel', '2017-08-11 01:47:18', '2017-08-11 01:47:18'),
(49, 11, 3, 'Inter i5', '2017-08-11 01:47:18', '2017-08-11 01:47:18'),
(50, 11, 8, '4', '2017-08-11 01:47:18', '2017-08-11 01:47:18'),
(51, 13, 4, '32', '2017-08-21 15:25:05', '2017-08-21 15:25:05'),
(52, 13, 5, 'Intel', '2017-08-21 15:25:05', '2017-08-21 15:25:05'),
(53, 13, 6, 'Intel i3', '2017-08-21 15:25:05', '2017-08-21 15:25:05'),
(54, 13, 7, '10', '2017-08-21 15:25:05', '2017-08-21 15:25:05'),
(55, 13, 9, '1', '2017-08-21 15:25:05', '2017-08-21 15:25:05');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `properties`
--

CREATE TABLE `properties` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `data_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `low_limit` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `high_limit` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `note` varchar(512) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` varchar(512) COLLATE utf8mb4_unicode_ci NOT NULL,
  `unit` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `properties`
--

INSERT INTO `properties` (`id`, `name`, `data_type`, `low_limit`, `high_limit`, `created_at`, `updated_at`, `note`, `value`, `unit`) VALUES
(1, 'Hdd', 'enum', '0', '100', '2017-08-11 01:22:42', '2017-08-11 01:22:42', 'Capacity of harddisk', '32;64;128;256;512;1024;2048', 'Gb'),
(2, 'CPU maker', 'enum', '0', '100', '2017-08-11 01:23:24', '2017-08-11 01:23:24', 'The maker of the CPU', 'Intel; AMD', ''),
(3, 'CPU serial', 'enum', '0', '100', '2017-08-11 01:25:19', '2017-08-11 01:25:19', 'Detail Cpu seria.', 'Intel i3; Inter i5; Intel i7; opteron 6300;opteron 6400;opteron 6700', ''),
(4, 'Screen Size', 'enum', '0', '100', '2017-08-11 01:25:47', '2017-08-11 01:25:47', 'Screen size', '10;12;13;14;15.6;17;18;18.5;19;20;21.5', 'inch'),
(5, 'Ram', 'enum', '0', '100', '2017-08-11 01:35:27', '2017-08-11 01:35:27', 'Memory capacity', '1;2;4;6;8;12;16;32', 'Gb');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Chỉ mục cho các bảng đã đổ
--

--
-- Chỉ mục cho bảng `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `category_properties`
--
ALTER TABLE `category_properties`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Chỉ mục cho bảng `photos`
--
ALTER TABLE `photos`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `product_category_properties`
--
ALTER TABLE `product_category_properties`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `properties`
--
ALTER TABLE `properties`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT cho các bảng đã đổ
--

--
-- AUTO_INCREMENT cho bảng `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT cho bảng `category_properties`
--
ALTER TABLE `category_properties`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT cho bảng `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT cho bảng `photos`
--
ALTER TABLE `photos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT cho bảng `products`
--
ALTER TABLE `products`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT cho bảng `product_category_properties`
--
ALTER TABLE `product_category_properties`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=56;
--
-- AUTO_INCREMENT cho bảng `properties`
--
ALTER TABLE `properties`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT cho bảng `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
